package app

import (
	"log"
	"net/http"

	"./controller"
	"github.com/gorilla/mux"
)

var myRouter *mux.Router

func run() {
	log.Println("Starting Server")
	log.Fatal(http.ListenAndServe(":3000", myRouter))
}

// InitializeRoutes initiatizes the routes available in the app
func InitializeRoutes() {
	myRouter = mux.NewRouter().StrictSlash(true)
	setRoutes(myRouter)
	run()
}

func setRoutes(myRouter *mux.Router) {
	log.Println("Initializing Routes")
	myRouter.HandleFunc("/", home)
	myRouter.HandleFunc("/employee", CreateEmployee).Methods("POST")
	myRouter.HandleFunc("/employee/{id}", UpdateEmployee).Methods("PUT")
	myRouter.HandleFunc("/employee/{id}", DeleteEmployee).Methods("DELETE")
	myRouter.HandleFunc("/employees", GetAllEmployees)
	myRouter.HandleFunc("/employee/{id}", GetEmployee)
}

func home(w http.ResponseWriter, r *http.Request) {
	controller.LoadHomePage(w, r)
}

// CreateEmployee creates new Employee
func CreateEmployee(w http.ResponseWriter, r *http.Request) {
	controller.CreateEmployee(w, r)
}

// GetAllEmployees get all the Employees
func GetAllEmployees(w http.ResponseWriter, r *http.Request) {
	controller.GetAllEmployees(w, r)
}

// GetEmployee gets an Employee on the basis of Id
func GetEmployee(w http.ResponseWriter, r *http.Request) {
	controller.GetEmployee(w, r)
}

// UpdateEmployee updates Employee on the basis of Id
func UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	controller.UpdateEmployee(w, r)
}

// DeleteEmployee deleted an Employee on the basis of Id
func DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	controller.DeleteEmployee(w, r)
}

/*
// Post handler methods
func (myRouter1 *mux.Router) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	fmt.Println(f)
	myRouter1.HandleFunc(path, f).Methods("POST")
}

// Get handler method
func (myRouter1 *mux.Router) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	myRouter1.HandleFunc(path, f).Methods("Get")
}

// Put handler method
func (myRouter1 *mux.Router) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	myRouter1.HandleFunc(path, f).Methods("Put")
}

// Delete handler method
func (myRouter1 *mux.Router) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	myRouter1.HandleFunc(path, f).Methods("Delete")
}
*/
