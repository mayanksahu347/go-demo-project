package main

import (
	"./app"
	"./config"
	"./migrate"
)

func main() {
	// initialize db
	config.OpenDbConnection()

	// migrate db
	migrate.DBMigrate(config.GetDB())

	// initialize app and routes
	app.InitializeRoutes()
}
