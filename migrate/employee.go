package migrate

import (
	"fmt"
	"log"

	"gorm.io/gorm"
)

// Employee table structure
type Employee struct {
	gorm.Model
	EmployeeID  int    `gorm:"primaryKey" "column:employee_id" json:"id"`
	Name        string `gorm:"column:name" json:"name"`
	Designation string `gorm:"column:designation" json:"designation"`
	Scale       string `gorm:column:scale" json:"scale"`
	BasicPay    int    `gorm:"column: basic_pay"`
}

// DBMigrate creates db migration for employees table
func DBMigrate(db *gorm.DB) *gorm.DB {
	log.Println("Auto Migrated Database")
	db.Migrator().AutoMigrate(&Employee{})
	hasUser := db.Migrator().HasTable(&Employee{})
	fmt.Println("Table Employee is", hasUser)
	if !hasUser {
		db.Migrator().CreateTable(&Employee{})
	}

	return db
}
