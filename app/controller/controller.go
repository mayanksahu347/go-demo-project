package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../models"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

// var db *gorm.DB = config.GetDB()
var db *gorm.DB

// LoadHomePage loads home page for the server
func LoadHomePage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: homePage")
	fmt.Fprintf(w, "Welcome to the HomePage!")
}

// CreateEmployee creates new employee
func CreateEmployee(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: Create Employee")
	decoder := json.NewDecoder(r.Body)
	// fmt.Printf("%T %T\n", r.Body, decoder) // (*http.body, *json.Decoder)

	var newEmployee models.Employee

	err := decoder.Decode(&newEmployee)
	if err != nil {
		fmt.Println("Some error occurred", err)
		fmt.Fprintf(w, "Failed to create new employee")
		return
	}

	// TODO: Add Validator for body parameter
	success := models.CreateEmployee(newEmployee)
	if success == 1 {
		// TODO: Create Response Package
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		fmt.Fprintf(w, "Employee successfully created: "+newEmployee.Name)
	} else {
		w.WriteHeader(200)
		fmt.Fprintf(w, "Failed to create new employee: "+newEmployee.Name)
	}

}

// GetAllEmployees Gets all employee
func GetAllEmployees(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: Get all Employee")
	employees := []models.Employee{}

	/* doubt
	var employees []models.Employee{}
	syntax error: non-declaration statement outside function body
	*/

	err := models.GetAllEmployees(&employees)
	if err != nil {
		return
	}

	response, err := json.Marshal(employees)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write([]byte(response))
}

// GetEmployee get single employee
func GetEmployee(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var employee models.Employee
	id, _ := strconv.Atoi(vars["id"])
	fmt.Println("Endpoint Hit: Get Employee", id)

	err := models.GetEmployee(id, &employee)
	if err != nil {
		w.WriteHeader(200)
		fmt.Println(err.Error())
		w.Write([]byte(err.Error()))
		return
	}

	response, err := json.Marshal(employee)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write([]byte("Employee Details\n"))
	w.Write([]byte(response))
}

// UpdateEmployee updates employee details
func UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: Update employee")
	vars := mux.Vars(r)
	var employee models.Employee
	fmt.Println("ammamasmasmamsamsamsmam", vars["id"])
	id, _ := strconv.Atoi(vars["id"])
	fmt.Println("ammamasmasmamsamsamsmam", id)

	err := models.GetEmployee(id, &employee)

	if err != nil {
		// record not found
		w.WriteHeader(200)
		w.Write([]byte("ABCD" + err.Error()))
		return
	}

	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&employee)
	if err != nil {
		fmt.Println("Error occurred while decoding json body:", id, err)
		w.WriteHeader(500)
		w.Write([]byte("Failed to update employee"))
		return
	}

	defer r.Body.Close()
	err = models.UpdateEmployee(&employee)
	if err != nil {
		fmt.Println("Error occurred while updating employee:", id, err)
		w.WriteHeader(500)
		w.Write([]byte("Failed to update employee"))
		return
	}

	response, err := json.Marshal(employee)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write([]byte("Employee updated\n"))
	w.Write([]byte(response))
}

// DeleteEmployee deletes employee details
func DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var employee models.Employee
	id, _ := strconv.Atoi(vars["id"])
	err := models.GetEmployee(id, &employee)
	if err != nil {
		// record not found
		w.WriteHeader(200)
		w.Write([]byte("Not Found: " + err.Error()))
		return
	}

	err = models.DeleteEmployee(&employee)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write([]byte("Employee Deleted\n"))
}
