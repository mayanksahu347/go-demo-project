package config

import (
	"log"
	"time"

	"github.com/tkanos/gonfig"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Configuration struct
type Configuration struct {
	Port       int
	DbUsername string
	DbPassword string
	DbDatabase string
}

var db *gorm.DB

var err error

// OpenDbConnection connects with mysql DB
func OpenDbConnection() {
	configuration, err := getConfig()
	if err != nil {
		return
	}
	dsn := configuration.DbUsername + ":" + configuration.DbPassword + "@tcp(127.0.0.1:3306)/" + configuration.DbDatabase + "?charset=utf8&parseTime=True"
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		NowFunc: func() time.Time {
			return time.Now().Local()
		}})

	if err != nil {
		log.Println("DB Connection Failed to Open")
	} else {
		log.Println("DB Connection Established with database:", configuration.DbDatabase)
	}
}

// GetDB returns db connection
func GetDB() *gorm.DB {
	return db
}

func getConfig() (*Configuration, error) {
	configuration := Configuration{}
	err := gonfig.GetConf("/home/vishal/work/training/go-training/demo-project/config/properties.json", &configuration)
	if err != nil {
		log.Println("Unable to load configuration")
		return nil, err
	}
	return &configuration, nil
}
