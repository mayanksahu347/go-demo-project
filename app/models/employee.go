package models

import (
	"fmt"

	"../../config"
	"gorm.io/gorm"
)

// Employee table structure
type Employee struct {
	gorm.Model
	EmployeeID  int    `gorm:"primaryKey" "->;<-:create" "column:employee_id" json:"employeeId"`
	Name        string `gorm:"column:name" json:"name"`
	Designation string `gorm:"column:designation" json:"designation"`
	Scale       string `gorm:column:scale" json:"scale"`
	BasicPay    int    `gorm:"column: basic_pay" json:"basicPay"`
}

// CreateEmployee creates new employee in the database
func CreateEmployee(employee Employee) int {
	db := config.GetDB()
	result := db.Create(&employee)
	if result.Error != nil {
		fmt.Println("Some error occurred", result.Error)
		return 0
	}

	fmt.Printf("Employee successfully created: " + employee.Name + "\n")
	return 1
}

// GetAllEmployees lists all the employees in the DB
func GetAllEmployees(employees *[]Employee) error {
	db := config.GetDB()
	// doubt why .Error
	if err := db.Order("id asc").Find(employees).Error; err != nil {
		return err
	}
	return nil
}

// GetEmployee returns one book on the basis of Id
func GetEmployee(id int, employee *Employee) (err error) {
	db := config.GetDB()
	/* doubt
	fmt.Println("amamamamams", employee)
	fmt.Println("amamamamams", &employee)
	amamamamams &{{0 0001-01-01 00:00:00 +0000 UTC 0001-01-01 00:00:00 +0000 UTC {0001-01-01 00:00:00 +0000 UTC false}} 0    0}
	amamamamams 0xc0000b6000

	*/
	if err := db.Where("employee_id = ?", id).First(employee).Error; err != nil {
		fmt.Println("Error Occurred", err)
		return err
	}

	return nil
}

// UpdateEmployee updates employee data in DB
// doubt db operation avoiding primary key update
func UpdateEmployee(employee *Employee) error {
	db := config.GetDB()
	// result := db.Model(&employee).Omit("EmployeeID").Updates(employee)
	if err := db.Save(employee).Error; err != nil {
		return err
	}
	// fmt.Println(employee)
	// fmt.Println(result.RowsAffected, result.Error)
	return nil
}

// DeleteEmployee deletes the employee
func DeleteEmployee(employee *Employee) error {
	db := config.GetDB()
	if err := db.Delete(employee).Error; err != nil {
		return err
	}
	return nil
}
